require 'test_helper'

class SchoolreviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @schoolreview = schoolreviews(:one)
  end

  test "should get index" do
    get schoolreviews_url
    assert_response :success
  end

  test "should get new" do
    get new_schoolreview_url
    assert_response :success
  end

  test "should create schoolreview" do
    assert_difference('Schoolreview.count') do
      post schoolreviews_url, params: { schoolreview: { comment: @schoolreview.comment, rating: @schoolreview.rating, references: @schoolreview.references, school: @schoolreview.school, user: @schoolreview.user } }
    end

    assert_redirected_to schoolreview_url(Schoolreview.last)
  end

  test "should show schoolreview" do
    get schoolreview_url(@schoolreview)
    assert_response :success
  end

  test "should get edit" do
    get edit_schoolreview_url(@schoolreview)
    assert_response :success
  end

  test "should update schoolreview" do
    patch schoolreview_url(@schoolreview), params: { schoolreview: { comment: @schoolreview.comment, rating: @schoolreview.rating, references: @schoolreview.references, school: @schoolreview.school, user: @schoolreview.user } }
    assert_redirected_to schoolreview_url(@schoolreview)
  end

  test "should destroy schoolreview" do
    assert_difference('Schoolreview.count', -1) do
      delete schoolreview_url(@schoolreview)
    end

    assert_redirected_to schoolreviews_url
  end
end
