require 'test_helper'

class SchoolpostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @schoolpost = schoolposts(:one)
  end

  test "should get index" do
    get schoolposts_url
    assert_response :success
  end

  test "should get new" do
    get new_schoolpost_url
    assert_response :success
  end

  test "should create schoolpost" do
    assert_difference('Schoolpost.count') do
      post schoolposts_url, params: { schoolpost: { content: @schoolpost.content, text: @schoolpost.text, text,: @schoolpost.text,, title: @schoolpost.title, view: @schoolpost.view } }
    end

    assert_redirected_to schoolpost_url(Schoolpost.last)
  end

  test "should show schoolpost" do
    get schoolpost_url(@schoolpost)
    assert_response :success
  end

  test "should get edit" do
    get edit_schoolpost_url(@schoolpost)
    assert_response :success
  end

  test "should update schoolpost" do
    patch schoolpost_url(@schoolpost), params: { schoolpost: { content: @schoolpost.content, text: @schoolpost.text, text,: @schoolpost.text,, title: @schoolpost.title, view: @schoolpost.view } }
    assert_redirected_to schoolpost_url(@schoolpost)
  end

  test "should destroy schoolpost" do
    assert_difference('Schoolpost.count', -1) do
      delete schoolpost_url(@schoolpost)
    end

    assert_redirected_to schoolposts_url
  end
end
