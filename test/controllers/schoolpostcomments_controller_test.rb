require 'test_helper'

class SchoolpostcommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @schoolpostcomment = schoolpostcomments(:one)
  end

  test "should get index" do
    get schoolpostcomments_url
    assert_response :success
  end

  test "should get new" do
    get new_schoolpostcomment_url
    assert_response :success
  end

  test "should create schoolpostcomment" do
    assert_difference('Schoolpostcomment.count') do
      post schoolpostcomments_url, params: { schoolpostcomment: { content: @schoolpostcomment.content, references: @schoolpostcomment.references, schoolpost: @schoolpostcomment.schoolpost, user: @schoolpostcomment.user } }
    end

    assert_redirected_to schoolpostcomment_url(Schoolpostcomment.last)
  end

  test "should show schoolpostcomment" do
    get schoolpostcomment_url(@schoolpostcomment)
    assert_response :success
  end

  test "should get edit" do
    get edit_schoolpostcomment_url(@schoolpostcomment)
    assert_response :success
  end

  test "should update schoolpostcomment" do
    patch schoolpostcomment_url(@schoolpostcomment), params: { schoolpostcomment: { content: @schoolpostcomment.content, references: @schoolpostcomment.references, schoolpost: @schoolpostcomment.schoolpost, user: @schoolpostcomment.user } }
    assert_redirected_to schoolpostcomment_url(@schoolpostcomment)
  end

  test "should destroy schoolpostcomment" do
    assert_difference('Schoolpostcomment.count', -1) do
      delete schoolpostcomment_url(@schoolpostcomment)
    end

    assert_redirected_to schoolpostcomments_url
  end
end
