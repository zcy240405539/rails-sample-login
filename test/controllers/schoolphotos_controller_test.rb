require 'test_helper'

class SchoolphotosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @schoolphoto = schoolphotos(:one)
  end

  test "should get index" do
    get schoolphotos_url
    assert_response :success
  end

  test "should get new" do
    get new_schoolphoto_url
    assert_response :success
  end

  test "should create schoolphoto" do
    assert_difference('Schoolphoto.count') do
      post schoolphotos_url, params: { schoolphoto: { view: @schoolphoto.view } }
    end

    assert_redirected_to schoolphoto_url(Schoolphoto.last)
  end

  test "should show schoolphoto" do
    get schoolphoto_url(@schoolphoto)
    assert_response :success
  end

  test "should get edit" do
    get edit_schoolphoto_url(@schoolphoto)
    assert_response :success
  end

  test "should update schoolphoto" do
    patch schoolphoto_url(@schoolphoto), params: { schoolphoto: { view: @schoolphoto.view } }
    assert_redirected_to schoolphoto_url(@schoolphoto)
  end

  test "should destroy schoolphoto" do
    assert_difference('Schoolphoto.count', -1) do
      delete schoolphoto_url(@schoolphoto)
    end

    assert_redirected_to schoolphotos_url
  end
end
