require 'test_helper'

class PhotocommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @photocomment = photocomments(:one)
  end

  test "should get index" do
    get photocomments_url
    assert_response :success
  end

  test "should get new" do
    get new_photocomment_url
    assert_response :success
  end

  test "should create photocomment" do
    assert_difference('Photocomment.count') do
      post photocomments_url, params: { photocomment: { content: @photocomment.content } }
    end

    assert_redirected_to photocomment_url(Photocomment.last)
  end

  test "should show photocomment" do
    get photocomment_url(@photocomment)
    assert_response :success
  end

  test "should get edit" do
    get edit_photocomment_url(@photocomment)
    assert_response :success
  end

  test "should update photocomment" do
    patch photocomment_url(@photocomment), params: { photocomment: { content: @photocomment.content } }
    assert_redirected_to photocomment_url(@photocomment)
  end

  test "should destroy photocomment" do
    assert_difference('Photocomment.count', -1) do
      delete photocomment_url(@photocomment)
    end

    assert_redirected_to photocomments_url
  end
end
