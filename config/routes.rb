Rails.application.routes.draw do

  resources :schoolposts
  resources :schoolphotos
  mount Ckeditor::Engine => '/ckeditor'
  resources :schools do
    get :search, :on => :collection
    # resources :posts
    member do
      get :showpost, :showmain, :showreview, :showphoto
    end
    resources :schoolreviews
    resources :schoolphotos
    resources :schoolposts
  end
  resources :schoolposts do
    resources :schoolpostcomments
  end
  resources :schoolpostcomments , only: [:index, :create]
  get '/schoolpostcomments/new/(:parent_id)', to: 'schoolpostcomments#new', as: :new_schoolpostcomment
  #
  resources :followers do
    member do
      get :userfollowschool, :userunfollowschool
    end
  end

  get '/pages/mypost', to: 'pages#mypost', as: :mypost_pages
  get '/pages/home', to: 'pages#home', as: :home_pages
  resources :schoolreviews
  resources :photocomments, only: [:index, :create]
  get '/photocomments/new/(:parent_id)', to: 'photocomments#new', as: :new_photocomment
  root 'pages#home'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  #get 'users/:id', to: 'users#posts'
  resources :users
  #set the root to /posts
  #get "posts/new"
  #get "posts/index"
  #get "posts/show"
  devise_for :users, path: '/account',
  path_names: { sign_in: 'login', sign_out: 'logout'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
