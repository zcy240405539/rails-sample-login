# CarrierWave.configure do |config|
#   config.storage              = :aliyun
#   config.aliyun_access_id     = ENV["LTAI5mm3LcmJZxpE"]
#   config.aliyun_access_key    = ENV["fWF6rBJtF8mLwegpRgt18jyPkqcSpq"]
#   config.aliyun_bucket        = ENV["haicaoyun"]
#   config.aliyun_host          = "https://haicaoyun.oss-cn-beijing.aliyuncs.com"   #oss 给我的外网域名，我没有自定义
#   config.aliyun_internal      = false
#   config.aliyun_area          = "cn-beijing"
#   config.aliyun_private_read  = true
#
# end
  #    region:                ENV['S3_REGION'],

CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'                        # required
  config.fog_credentials = {
    provider:              'AWS',                        # required
    aws_access_key_id:     ENV["AWS_ACCESS_KEY"],        # required
    aws_secret_access_key: ENV["AWS_SECRET_KEY"],        # required
    region:                ENV['S3_REGION'],
  }
  config.fog_directory  = ENV["AWS_BUCKET"]              # required
end
