Ckeditor::Asset.create!([
  {data_file_name: "IMG_3110.JPG", data_content_type: "image/jpeg", data_file_size: 2550121, type: "Ckeditor::Picture", width: 4032, height: 3024}
])
PhotocommentHierarchy.create!([
  {ancestor_id: 4, descendant_id: 4, generations: 0},
  {ancestor_id: 5, descendant_id: 5, generations: 0},
  {ancestor_id: 4, descendant_id: 5, generations: 1},
  {ancestor_id: 6, descendant_id: 6, generations: 0},
  {ancestor_id: 4, descendant_id: 6, generations: 1},
  {ancestor_id: 7, descendant_id: 7, generations: 0},
  {ancestor_id: 6, descendant_id: 7, generations: 1},
  {ancestor_id: 4, descendant_id: 7, generations: 2},
  {ancestor_id: 8, descendant_id: 8, generations: 0},
  {ancestor_id: 9, descendant_id: 9, generations: 0},
  {ancestor_id: 7, descendant_id: 9, generations: 1},
  {ancestor_id: 6, descendant_id: 9, generations: 2},
  {ancestor_id: 4, descendant_id: 9, generations: 3},
  {ancestor_id: 10, descendant_id: 10, generations: 0},
  {ancestor_id: 11, descendant_id: 11, generations: 0},
  {ancestor_id: 12, descendant_id: 12, generations: 0},
  {ancestor_id: 13, descendant_id: 13, generations: 0},
  {ancestor_id: 6, descendant_id: 13, generations: 1},
  {ancestor_id: 4, descendant_id: 13, generations: 2},
  {ancestor_id: 14, descendant_id: 14, generations: 0},
  {ancestor_id: 13, descendant_id: 14, generations: 1},
  {ancestor_id: 6, descendant_id: 14, generations: 2},
  {ancestor_id: 4, descendant_id: 14, generations: 3}
])
Photocomment.create!([
  {content: "testcomment1", parent_id: nil},
  {content: "testcomment2", parent_id: nil},
  {content: "test2", parent_id: 1},
  {content: "12", parent_id: 1},
  {content: "12 reply", parent_id: 4},
  {content: "12 reply1", parent_id: 4},
  {content: "12 reply 12", parent_id: 6},
  {content: "dadda", parent_id: 1},
  {content: "1212", parent_id: 7},
  {content: "w", parent_id: nil},
  {content: "w1", parent_id: nil},
  {content: "w2", parent_id: nil},
  {content: "12 reply 11", parent_id: 6},
  {content: "12 reply 1111", parent_id: 13}
])
School.create!([

  {name: "The University of Texas at Tyler", cnname: "德克萨斯大学泰勒分校", schoolintro: "<p style=\"text-align: center;\"><iframe allow=\"autoplay; encrypted-media\" allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"https://www.youtube.com/embed/geLIh7y7qAM\" width=\"560\"></iframe></p>\r\n\r\n<p center=\"\" style=\"\\&quot;text-align:\"><strong>德克萨斯大学泰勒分校（UT Tyler）是一所位于美国德克萨斯州泰勒的公立大学</strong></p>\r\n", schoolimage: "uttyler.jpg"},
  {name: "University of Texas at Dallas", cnname: "", schoolintro: "", schoolimage: nil}
])
Schoolcomment.create!([
  {content: "ewply", user_id: 1, schoolpost_id: 1, parent_id: nil}
])
Schoolphoto.create!([
  {view: nil, photo: "IMG_3103.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3109.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3111.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3178.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3188.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3110.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3110.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3110.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3179.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3110.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3110.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3179.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: nil, school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3178.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3111.JPG", school_id: 1, user_id: 1},
  {view: nil, photo: "IMG_3776.JPG", school_id: 1, user_id: 1}
])
Schoolpost.create!([
  {view: nil, title: "Post1", content: "", school_id: 1, user_id: 1},
  {view: 1, title: "post2", content: "test1", school_id: 1, user_id: 1},
  {view: nil, title: "post3", content: "nkk", school_id: 1, user_id: 1},
  {view: nil, title: "post4", content: "ass", school_id: 1, user_id: 1},
  {view: nil, title: "post5", content: "ass", school_id: 1, user_id: 1},
  {view: 0, title: "post6", content: "ass", school_id: 1, user_id: 1},
  {view: 1, title: "cao", content: "dadawdaw", school_id: 1, user_id: 1}
])
Schoolreview.create!([
  {rating: nil, comment: "Very nice school", school_id: 1, user_id: 1},
  {rating: nil, comment: "学校环境优美", school_id: 1, user_id: 1},
  {rating: nil, comment: "sdfsdfs", school_id: 1, user_id: 1},
  {rating: nil, comment: "dfs", school_id: 1, user_id: 1}
])
Ckeditor::Picture.create!([
  {data_file_name: "IMG_3110.JPG", data_content_type: "image/jpeg", data_file_size: 2550121, type: "Ckeditor::Picture", width: 4032, height: 3024}
])
