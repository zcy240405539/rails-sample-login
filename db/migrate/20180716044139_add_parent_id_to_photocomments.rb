class AddParentIdToPhotocomments < ActiveRecord::Migration[5.2]
  def change
    add_column :photocomments, :parent_id, :integer
  end
end
