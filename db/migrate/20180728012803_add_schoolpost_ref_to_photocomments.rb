class AddSchoolpostRefToPhotocomments < ActiveRecord::Migration[5.2]
  def change
    add_reference :photocomments, :schoolpost, foreign_key: true
    add_reference :photocomments, :user, foreign_key: true
  end
end
