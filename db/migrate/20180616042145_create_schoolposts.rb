class CreateSchoolposts < ActiveRecord::Migration[5.2]
  def change
    create_table :schoolposts do |t|
      t.integer :view
      t.string :title
      t.text :content
      t.references :school, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
