class CreateSchoolphotos < ActiveRecord::Migration[5.2]
  def change
    create_table :schoolphotos do |t|
      t.integer :view
      t.string :photo
      t.references :school, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
