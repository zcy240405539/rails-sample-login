class AddPosttopicRefToSchoolpost < ActiveRecord::Migration[5.2]
  def change
    remove_column :schoolposts, :posttype
    add_reference :schoolposts, :posttopic, foreign_key: true
  end
end
