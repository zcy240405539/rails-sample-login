class AddPosttypeToSchoolpost < ActiveRecord::Migration[5.2]
  def change
    add_column :schoolposts, :posttype, :string
  end
end
