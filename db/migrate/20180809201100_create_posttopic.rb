class CreatePosttopic < ActiveRecord::Migration[5.2]
  def change
    create_table :posttopics do |t|
      t.string :topicname
      t.timestamps
    end
  end
end
