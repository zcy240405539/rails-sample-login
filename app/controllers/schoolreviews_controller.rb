class SchoolreviewsController < ApplicationController
  before_action :set_schoolreview, only: [:show, :edit, :update, :destroy]

  # GET /schoolreviews
  # GET /schoolreviews.json
  def index
    @schoolreviews = Schoolreview.all
  end

  # GET /schoolreviews/1
  # GET /schoolreviews/1.json
  def show
  end

  # GET /schoolreviews/new
  def new
    @schoolreview = Schoolreview.new
  end

  # GET /schoolreviews/1/edit
  def edit
  end

  # POST /schoolreviews
  # POST /schoolreviews.json
  def create

    @school = School.find(params[:school_id])
    @schoolreview = @school.schoolreviews.new(schoolreview_params)
    if @schoolreview.save
      flash[:success] = "Success!"
      redirect_to controller: 'schools', action: 'showreview', id: params[:school_id]
    else
      flash[:error] = @schoolreview.errors.full_messages
      redirect_to new_schoolreview_path
    end
  end

  # PATCH/PUT /schoolreviews/1
  # PATCH/PUT /schoolreviews/1.json
  def update
    respond_to do |format|
      if @schoolreview.update(schoolreview_params)
        format.html { redirect_to @schoolreview, notice: 'Schoolreview was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoolreview }
      else
        format.html { render :edit }
        format.json { render json: @schoolreview.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schoolreviews/1
  # DELETE /schoolreviews/1.json
  def destroy
    @schoolreview.destroy
    respond_to do |format|
      format.html { redirect_to schoolreviews_url, notice: 'Schoolreview was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoolreview
      @schoolreview = Schoolreview.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoolreview_params
      params.require(:schoolreview).permit(:rating, :comment, :user_id, :school_id)
    end
end
