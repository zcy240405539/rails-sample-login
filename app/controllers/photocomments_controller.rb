class PhotocommentsController < ApplicationController
  before_action :set_photocomment, only: [:show, :edit, :update, :destroy]

  # GET /photocomments
  # GET /photocomments.json
  def index
    @photocomments = Photocomment.hash_tree
  end

  # GET /photocomments/1
  # GET /photocomments/1.json
  def show
  end

  # GET /photocomments/new
  def new
    @photocomment = Photocomment.new(parent_id: params[:parent_id])
  end

  # GET /photocomments/1/edit
  def edit
  end

  # POST /photocomments
  # POST /photocomments.json
  def create

    if params[:photocomment][:parent_id].present?
        parent = Photocomment.find_by_id(params[:photocomment].delete(:parent_id))
        @photocomment = parent.children.build(photocomment_params)
      else
        @photocomment = Photocomment.new(photocomment_params)
      end

      if @photocomment.save
        flash[:success] = 'Your comment was successfully added!'
        redirect_to root_url
      else
        render 'new'
      end

  end

  # PATCH/PUT /photocomments/1
  # PATCH/PUT /photocomments/1.json
  def update
    respond_to do |format|
      if @photocomment.update(photocomment_params)
        format.html { redirect_to @photocomment, notice: 'Photocomment was successfully updated.' }
        format.json { render :show, status: :ok, location: @photocomment }
      else
        format.html { render :edit }
        format.json { render json: @photocomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photocomments/1
  # DELETE /photocomments/1.json
  def destroy
    @photocomment.destroy
    respond_to do |format|
      format.html { redirect_to photocomments_url, notice: 'Photocomment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photocomment
      @photocomment = Photocomment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photocomment_params
      params.require(:photocomment).permit(:content,:parent_id)
    end
end
