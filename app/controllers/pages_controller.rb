class PagesController < ApplicationController
  # before_action :authenticate_user!
  def home
      @schools = School.all
      if @current_user
        @schoolfollowings = @current_user.following_by_type('School')
        @schoolrecs = School.where.not(id: @schoolfollowings.ids)
      else
        @schoolrecs = School.all
      end
      if params[:posttype]
        @Schoolposts = Schoolpost.where(posttopic_id: params[:posttype])
      else
        @Schoolposts = Schoolpost.all
      end
      @posttopics = Posttopic.where(id: Schoolpost.distinct.pluck(:posttopic_id))
  end

  def mypost
      @schools = School.all
      if @current_user
        @schoolfollowings = @current_user.following_by_type('School')
        @schoolrecs = School.where.not(id: @schoolfollowings.ids)
        @posttopics = Posttopic.where(id: Schoolpost.where(school_id: @schoolfollowings.ids).distinct.pluck(:posttopic_id))
        @posttopics = Posttopic.where(id: Schoolpost.where(school_id: @schoolfollowings.ids).distinct.pluck(:posttopic_id))
        if params[:posttype]
          @Schoolposts = Schoolpost.where(posttopic_id: params[:posttype], school_id: @schoolfollowings.ids)
        else
          @Schoolposts = Schoolpost.where(school_id: @schoolfollowings.ids)
        end
      else
        @schoolrecs = School.all
      end


      render 'home'
  end



  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_params
      params.permit(:posttype)
    end



end
