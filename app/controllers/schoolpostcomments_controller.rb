class SchoolpostcommentsController < ApplicationController
  before_action :set_schoolpostcomment, only: [:show, :edit, :update, :destroy]

  # GET /schoolpostcomments
  # GET /schoolpostcomments.json
  def index
    @schoolpostcomments = Schoolpostcomment.hash_tree
  end

  # GET /schoolpostcomments/1
  # GET /schoolpostcomments/1.json
  def show
  end

  # GET /schoolpostcomments/new
  def new
  @schoolpostcomment = Schoolpostcomment.new(parent_id: params[:parent_id])
  @parent = Schoolpostcomment.find(params[:parent_id])
  respond_to do |format|
    format.js {}
  end
    #@schoolpostcomment = Schoolpostcomment.new(parent_id: params[:parent_id])
  end

  # GET /schoolpostcomments/1/edit
  def edit
  end

  # POST /schoolpostcomments
  # POST /schoolpostcomments.json
  def create
    if params[:schoolpostcomment][:parent_id].present?
      parent = Schoolpostcomment.find_by_id(params[:schoolpostcomment][:parent_id])
      @schoolpostcomment = parent.children.build(schoolpostcomment_params)
      @schoolpostcomment.user = current_user
      @schoolpostcomment.schoolpost = parent.schoolpost
    else
      @schoolpostcomment = Schoolpostcomment.new(schoolpostcomment_params)
      @schoolpostcomment.schoolpost = Schoolpost.find(params[:schoolpost_id])
      @schoolpostcomment.user = current_user
    end
    #@schoolpostcomment = current_user.schoolpostcomments.new(schoolpostcomment_params)
    if @schoolpostcomment.save
      if params[:schoolpostcomment][:parent_id].present?
        redirect_to controller: 'schoolposts', action: 'show', id: parent.schoolpost.id
      else
        redirect_to controller: 'schoolposts', action: 'show', id: params[:schoolpost_id]
      end
    else
      flash[:error] = @schoolpostcomment.errors.full_messages
      redirect_to new_schoolreview_path
    end

  end

  # PATCH/PUT /schoolpostcomments/1
  # PATCH/PUT /schoolpostcomments/1.json
  def update
    respond_to do |format|
      if @schoolpostcomment.update(schoolpostcomment_params)
        format.html { redirect_to @schoolpostcomment, notice: 'Schoolpostcomment was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoolpostcomment }
      else
        format.html { render :edit }
        format.json { render json: @schoolpostcomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schoolpostcomments/1
  # DELETE /schoolpostcomments/1.json
  def destroy
    @schoolpostcomment.destroy
    respond_to do |format|
      format.html { redirect_to schoolpostcomments_url, notice: 'Schoolpostcomment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoolpostcomment
      @schoolpostcomment = Schoolpostcomment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoolpostcomment_params
      params.require(:schoolpostcomment).permit(:content,:parent_id)
    end
end
