class FollowersController < ApplicationController


	def userfollowschool
		 @school = School.find(params[:id])
		 @current_user.follow(@school)
		respond_to do |format|
	    format.js {}
	  end
  end

	def userunfollowschool
		 @school = School.find(params[:id])
		 @current_user.stop_following(@school)
		respond_to do |format|
	    format.js {}
	  end
  end

	private

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_params
      params.require(:follower).permit(:id)
    end

end
