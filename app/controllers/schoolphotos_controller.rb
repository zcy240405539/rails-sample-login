class SchoolphotosController < ApplicationController
  before_action :set_schoolphoto, only: [:show, :edit, :update, :destroy]

  # GET /schoolphotos
  # GET /schoolphotos.json
  def index
    @schoolphotos = Schoolphoto.all
  end

  # GET /schoolphotos/1
  # GET /schoolphotos/1.json
  def show
    @school = School.find(params[:school_id])
    @schoolphoto = @school.schoolphotos.find(params[:id])
  end

  # GET /schoolphotos/new
  def new
    #@schoolphoto = Schoolphoto.new
    @school = School.find(params[:school_id])
    @schoolphoto = @school.schoolphoto.build
  end

  # GET /schoolphotos/1/edit
  def edit
  end

  # POST /schoolphotos
  # POST /schoolphotos.json
  def create
    @school = School.find(params[:school_id])
    @schoolphoto = @school.schoolphotos.new(schoolphoto_params)

      if @schoolphoto.save
        redirect_to controller: 'schools', action: 'showphoto', id: params[:school_id]
      else
        format.html { render :new }
      end
  end

  # PATCH/PUT /schoolphotos/1
  # PATCH/PUT /schoolphotos/1.json
  def update
    respond_to do |format|
      if @schoolphoto.update(schoolphoto_params)
        format.html { redirect_to @schoolphoto, notice: 'Schoolphoto was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoolphoto }
      else
        format.html { render :edit }
        format.json { render json: @schoolphoto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schoolphotos/1
  # DELETE /schoolphotos/1.json
  def destroy
    @schoolphoto.destroy
    respond_to do |format|
      format.html { redirect_to schoolphotos_url, notice: 'Schoolphoto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoolphoto
      @schoolphoto = Schoolphoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoolphoto_params
      params.require(:schoolphoto).permit(:view, :photo, :user_id, :school_id)
    end
end
