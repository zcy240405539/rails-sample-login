class SchoolpostsController < ApplicationController
  before_action :set_schoolpost, only: [:show, :edit, :update, :destroy]
  layout "school",except: [:index, :new]

  # GET /schoolposts
  # GET /schoolposts.json
  def index
    @schoolposts = Schoolpost.all
  end

  # GET /schoolposts/1
  # GET /schoolposts/1.json
  def show
    #1st you retrieve the post thanks to params[:post_id]

  #school = School.find(params[:school_id])
  #2nd you retrieve the comment thanks to params[:id]

  #@schoolpost = school.schoolposts.find(params[:id])
  #@school = School.find(params[:school_id])
  #@schoolpost = @school.schoolposts.find(params[:id])
  @schoolpost = Schoolpost.find(params[:id])
  @school = @schoolpost.school
  @photocomments = @schoolpost.photocomments.hash_tree
  @schoolpostcomments = @schoolpost.schoolpostcomments.hash_tree
  @schoolpostcomment = Schoolpostcomment.new
    # @schoolcomment = Schoolcomment.new
    # @schoolcomments = @schoolpost.schoolcomments
  end

  # GET /schoolposts/new
  def new
    if params[:school_id]
      @school = School.find(params[:school_id])
      render layout: "school"
    else
      @schoolpost = Schoolpost.new
      @schoolfollow = @current_user.following_by_type('School')
      render layout: "application"
    end

  end

  # GET /schoolposts/1/edit
  def edit
  end

  # POST /schoolposts
  # POST /schoolposts.json
  def create
    if params[:school_id]
      @school = School.find(params[:school_id])
      @schoolpost = @school.schoolposts.new(schoolpost_params)
    else
      @schoolpost = Schoolpost.new(schoolpost_params)
    end
    if @schoolpost.save
      flash[:success] = "Success!"
      redirect_to controller: 'schools', action: 'showpost', id: params[:school_id].nil? ? schoolpost_params[:school_id] : params[:school_id]
    else
      flash[:error] = @schoolpost.errors.full_messages
      redirect_to new_school_schoolpost_path
    end

  end

  # PATCH/PUT /schoolposts/1
  # PATCH/PUT /schoolposts/1.json
  def update
    respond_to do |format|
      if @schoolpost.update(schoolpost_params)
        format.html { redirect_to @schoolpost, notice: 'Schoolpost was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoolpost }
      else
        format.html { render :edit }
        format.json { render json: @schoolpost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schoolposts/1
  # DELETE /schoolposts/1.json
  def destroy
    @schoolpost.destroy
    respond_to do |format|
      format.html { redirect_to schoolposts_url, notice: 'Schoolpost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoolpost
      @schoolpost = Schoolpost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoolpost_params
      params.require(:schoolpost).permit(:view, :title, :content, :posttopic_id, :user_id, :school_id)
    end
end
