json.extract! photocomment, :id, :content, :created_at, :updated_at
json.url photocomment_url(photocomment, format: :json)
