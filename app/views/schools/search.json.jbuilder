json.array!(@schools) do |school|
  json.name        school.name
  json.id          school.id
end
