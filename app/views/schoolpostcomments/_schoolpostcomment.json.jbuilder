json.extract! schoolpostcomment, :id, :content, :user, :references, :schoolpost, :references, :created_at, :updated_at
json.url schoolpostcomment_url(schoolpostcomment, format: :json)
