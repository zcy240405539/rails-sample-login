json.extract! schoolreview, :id, :rating, :comment, :user, :references, :school, :references, :created_at, :updated_at
json.url schoolreview_url(schoolreview, format: :json)
