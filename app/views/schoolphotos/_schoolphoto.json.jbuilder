json.extract! schoolphoto, :id, :view, :created_at, :updated_at
json.url schoolphoto_url(schoolphoto, format: :json)
