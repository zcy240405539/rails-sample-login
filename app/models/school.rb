class School < ApplicationRecord
  has_many :schoolposts
  has_many :schoolreviews
  has_many :schoolphotos
  mount_uploader :schoolimage, SchoolimageUploader
  #关注
  acts_as_followable

end
