class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :schoolposts
  has_many :schoolreviews
  has_many :schoolphotos
  has_many :schoolpostcomments
  has_many :photocomments
  #关注
  acts_as_follower
  def name
    self.nickname
  end
end
