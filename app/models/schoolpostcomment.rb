class Schoolpostcomment < ApplicationRecord
  belongs_to :schoolpost
  belongs_to :user
  has_closure_tree
end
