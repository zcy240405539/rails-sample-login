class Schoolpost < ApplicationRecord
  belongs_to :school
  belongs_to :user
  belongs_to :posttopic
  has_many :schoolpostcomments
  has_many :photocomments#, as: :commentable
end
