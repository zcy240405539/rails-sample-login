class Schoolphoto < ApplicationRecord
  belongs_to :school
  belongs_to :user
  mount_uploader :photo, PhotoUploader
end
