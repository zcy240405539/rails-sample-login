$(document).on('turbolinks:load',function(){
if ($( ".schools-search" ).length ) {
    new app.schools;
}
$("#lightgallery").lightGallery({
  thumbnail: false,
  speed: 0,
  download: false,
  fullScreen:false,
  autoplayControls: false,
  zoom: false,
  actualSize: false,
  share:false,
  counter:false,
});
})
var app = window.app = {};
app.schools = function() {
  this._input = $('#schools-search-txt');
  this._initAutocomplete();
};

app.schools.prototype = {
  _initAutocomplete: function() {
    this._input
      .autocomplete({
        source: '/schools/search',
        appendTo: '#schools-search-results',
        select: $.proxy(this._select, this),
        messages: {
            noResults: '',
            results: function() {}
        }
      })
      .autocomplete('instance')._renderItem = $.proxy(this._render, this);
  },

  _render: function(ul, item) {
    var markup = [
      '<div class="schoollist"><a href="/schools/'+ item.id + '">'+ item.name + '</a></div>'
    ];
    return $('<li>')
      .append(markup.join(''))
      .appendTo(ul);

  },

  _select: function(e, ui) {
    this._input.val(ui.item.name);
    return false;
  }
};
