module SchoolpostcommentsHelper
  def schoolpostcomments_tree_for(schoolpostcomments)
  schoolpostcomments.map do |schoolpostcomment, nested_schoolpostcomments|
    render(schoolpostcomment) +
        (nested_schoolpostcomments.size > 0 ? content_tag(:div, schoolpostcomments_tree_for(nested_schoolpostcomments), class: "replies") : nil)
  end.join.html_safe
end
end
