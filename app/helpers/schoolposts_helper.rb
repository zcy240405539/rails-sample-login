module SchoolpostsHelper
  def grouped_communities_options_for_select(selected_conversation_id = nill,communities)
    options = []
    # communities.all.map do |c|
    #     options.push([c.communitytype, [c.id, c.name]])
    # end

    community_by_type = communities.all.group_by(&:communitytype)

    community_by_type.each do |communitytype, communities|
      options1 = []
      communities.each do |community|
        options1.push([community.cnname, community.id])
      end
      options.push([communitytype,options1])
    end


    grouped_options_for_select(options)
    #render(options)
  end
end
