module PhotocommentsHelper
  def photocomments_tree_for(photocomments)
  photocomments.map do |photocomment, nested_photocomments|
    render(photocomment) +
        (nested_photocomments.size > 0 ? content_tag(:div, photocomments_tree_for(nested_photocomments), class: "replies") : nil)
  end.join.html_safe
end
end
